#pragma once

#include "UseSpell.generated.h"

UCLASS()
class UseSpell
{
public:
	virtual void accept(IVisitor& v) = 0;
};