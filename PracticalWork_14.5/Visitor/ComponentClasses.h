#pragma once

#include "UseSpell.h"
#include "ComponentClasses.generated.h"

UCLASS()
class UHealthComponent : public UseSpell
{
public:
	void accept(ISelfSpell& selfSpell) override { selfSpell.UseSelfSpell(*this); }
	void IncreaseHealth(float value) { Health += value }

private:
	float Health = 100.0f;
};

UCLASS()
class UManaComponent : public UseSpell
{
public:
	void accept(ISelfSpell& selfSpell) override { selfSpell.UseSelfSpell(*this); }
	void IncreaseMana(float value) { Mana += value }

private:
	float Mana = 100.0f;
};

UCLASS()
class UArmorComponent : public UseSpell
{
public:
	void accept(ISelfSpell& selfSpell) override { selfSpell.UseSelfSpell(*this); }
	void IncreaseArmor(float value) { Armor += value }

private:
	float Armor = 100.0f;
};