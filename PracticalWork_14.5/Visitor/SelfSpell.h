#pragma once

#include "SelfSpell.generated.h"

UCLASS()
class ISelfSpell
{
public:
	virtual void UseSelfSpell(HealthComponent& ref) = 0;
	virtual void UseSelfSpell(ManaComponent& ref) = 0;
	virtual void UseSelfSpell(ArmorComponent& ref) = 0;
};