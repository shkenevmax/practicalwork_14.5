#pragma once

#include "SelfSpell.h"
#include "SpellArcanFury.generated.h"

UCLASS()
class USpellArcanFury : public ISelfSpell
{
public:
	void UseSelfSpell(UHealthComponent& ref) override { ref.IncreaseHealth(50.0f); }
	void UseSelfSpell(UManaComponent& ref) override { ref.IncreaseMana(50.0f); }
	void UseSelfSpell(UArmorComponent& ref) override { ref.IncreaseArmor(50.0f); }

	void UseSpellArcanFury();
};

inline void SpellArcanFury::UseSpellArcanFury()
{
	UHealthComponent HealthComponent;
	UManaComponent ManaComponent;
	UArmorComponent ArmorComponent;

	UseSpell* Spells[] = { &HealthComponent, &ManaComponent, &ArmorComponent };

	for (auto Spell : Spells)
	{
		USpellArcanFury SpellArcanFury;
		Spell->accept(SpellArcanFury);
	}
}