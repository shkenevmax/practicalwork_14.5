#pragma once

#include "Command.h"
#include "InputHandler.generated.h"

UCLASS()
class UInputHandler
{
public:
	Command* HandleInput();
	// command binding functions

private:
	// pointers with linked actions
	Command* 1_Action; // ManaShieldCommand
	Command* 2_Action; // FireStormCommand
	Command* 3_Action; // ArcanBlastCommand
	Command* 4_Action; // FrostNovaCommand
};

inline Command* UInputHandler::HandleInput()
{
	if (isPressed(Button_1)) return 1_Action;
	if (isPressed(Button_2)) return 2_Action;
	if (isPressed(Button_3)) return 3_Action;
	if (isPressed(Button_4)) return 4_Action;

	return nullptr;
}