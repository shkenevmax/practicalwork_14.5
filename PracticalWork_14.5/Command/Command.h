#pragma once

#include "GameFramework/Actor.h"
#include "Command.generated.h"

// class command with a single virtual function

UCLASS()
class Command
{
public:
	virtual ~Command(){ }
	virtual void Execute(AActor& Actor) = 0;
};