#pragma once

#include "Command.h"
#include "GameFramework/Actor.h"
#include "ActionClasses.generated.h"

// functions of spells or abilities triggered by the command

UCLASS()
class ManaShieldCommand : public Command
{
public:
	void Execute(AActor& Actor) override { Actor.ManaShield(); }
};

UCLASS()
class FireStormCommand : public Command
{
public:
	void Execute(AActor& Actor) override { Actor.FireStorm(); }
};

UCLASS()
class ArcanBlastCommand : public Command
{
public:
	void Execute(AActor& Actor) override { Actor.ArcanBlast(); }
};

UCLASS()
class FrostNovaCommand : public Command
{
public:
	void Execute(AActor& Actor) override { Actor.FrostNova(); }
};