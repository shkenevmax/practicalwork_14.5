#pragma once

#include "AnimalBase.h"
#include "ChickenSqueal.h"
#include "Chicken.generated.h"

// the class of a specific animal to which we assign the behavior

UCLASS()
class UChicken : UAnimalBase
{
	GENERATED_BODY()

public:
	void ChickenSqueal()
	{
		UChickenSqueal ChickenSqueal;
		SetSquealBehavior(ChickenSqueal);
		Squeal();
	}
};