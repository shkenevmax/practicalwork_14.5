#pragma once

#include "AnimalBase.h"
#include "ScorpionSqueal.h"
#include "Scorpion.generated.h"

// the class of a specific animal to which we assign the behavior

UCLASS()
class UScorpion : UAnimalBase
{
	GENERATED_BODY()

public:
	void ScorpionSqueal()
	{
		UScorpionSqueal ScorpionSqueal;
		SetSquealBehavior(ScorpionSqueal);
		Squeal();
	}
};