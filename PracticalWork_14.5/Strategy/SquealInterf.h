#pragma once

#include "SquealInterf.generated.h"

// define the main behavior interface

UINTERFACE()
class USquealInterf : public UInterface
{
    GENERATED_BODY()

public:
    virtual void Squealing() = 0;
};