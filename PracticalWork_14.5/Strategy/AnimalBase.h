#pragma once

#include "SquealInterf.h"
#include "RunAwayInterf.h"
#include "DeadInterf.h"
#include "AnimalBase.generated.h"

UCLASS()
class UAnimalBase
{
	GENERATED_BODY()

protected:
	// define the variables and main methods of the base class

	USquealInterf SquealInterf;
	URunAwayInterf RunAwayInterf;
	UDeadInterf DeadInterf;

public:
	// methods of assigning behavior

	void SetSquealBehavior(USquealInterf newSqueal)
	{
		SquealInterf = newSqueal;
	}

	void SetRunAwayBehavior(URunAwayInterf newRunAway)
	{
		RunAwayInterf = newRunAway;
	}

	void SetDeadBehavior(UDeadInterf newDead)
	{
		DeadInterf = newDead;
	}

protected:
	// behavioral methods

	void Squeal()
	{
		SquealInterf.Squealing();
	}

	void RunAway()
	{
		RunAwayInterf.RunAway();
	}

	void Dead()
	{
		DeadInterf.Dead();
	}
};