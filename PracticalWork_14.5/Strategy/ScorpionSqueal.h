#pragma once

#include "SquealInterf.h"
#include "ScorpionSqueal.generated.h"

UCLASS()
class UScorpionSqueal : SquealInterf
{
	GENERATED_BODY()

public:
	// implement the constructor and methods related to the specified behavior

	void UScorpionSqueal() {}
	void Squealing() override { /*the scorpion squeals shrilly*/ }
	// other methods of behavior
};