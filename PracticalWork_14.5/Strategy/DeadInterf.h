#pragma once

#include "DeadInterf.generated.h"

// define the main behavior interface

UINTERFACE()
class UDeadInterf : public UInterface
{
    GENERATED_BODY()

public:
    virtual void Dead() = 0;
};