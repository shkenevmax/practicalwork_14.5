#pragma once

#include "SquealInterf.h"
#include "ChickenSqueal.generated.h"

UCLASS()
class UChickenSqueal : SquealInterf
{
	GENERATED_BODY()

public:
	// implement the constructor and methods related to the specified behavior

	void UChickenSqueal() {}
	void Squealing() override { /*the chicken squeals shrilly*/ }
	// other methods of behavior
};