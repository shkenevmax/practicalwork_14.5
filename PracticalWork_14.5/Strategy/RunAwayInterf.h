#pragma once

#include "RunAwayInterf.generated.h"

// define the main behavior interface

UINTERFACE()
class URunAwayInterf : public UInterface
{
    GENERATED_BODY()

public:
    virtual void RunAway() = 0;
};