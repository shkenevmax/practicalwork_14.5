#pragma once

#include "AIEnemy.h"
#include "MoveState.generated.h"

class AAIEnemy;
class AAIController;

// suppose we already have an artificial intelligence class to control the enemy

UENUM(BlueprintType)
enum EMoveState
{
	EMS_LookingForTarget = 0;
	EMS_Walk = 1;
	EMS_Attack = 2;
};

USTRUCT(Blueprintable, BlueprintType)
struct FMoveState
{
	GENERATED_BODY()

public:
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FStateChanged, FMoveState, NewState);
	FStateChanged OnStateChanged;

public:
	explicit FMoveState(EMoveState Type, APlayerController PlayerController)
	{
		LoadStateInfo();
	}

	FMoveState() = default;
	virtual ~FMoveState() = default;

	FMoveState GetStateType() const;

	virtual void ApplyState() {}

protected:
	virtual void LoadStateInfo();

private:
	FMoveState StateType;
};

USTRUCT()
struct FLookingForTargetState : public FMoveState
{
	GENERATED_BODY()

public:
	//the enemy turns and looks for the player
	void RotateToFindPlayer(AAIEnemy* Enemy);
};

USTRUCT()
struct FWalkState : public FMoveState
{
	GENERATED_BODY()

public:
	// the enemy walks to a random point, within a circle accessible to him and defined in advance
	void WalkToRundomPoint(AAIEnemy* Enemy);
};

USTRUCT()
struct FAttackState : public FMoveState
{
	GENERATED_BODY()

public:
	// we attack the player with what is in the arsenal
	void AttackPlayer(AAIEnemy* Enemy);
};