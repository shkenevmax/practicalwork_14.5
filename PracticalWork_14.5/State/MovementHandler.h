#pragma once

#include "Components/ActorComponent.h"
#include "MoveState.h"
#include "AIEnemy.h"
#include "MovementHandler.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))
class UMovementHandler : public UActorComponent
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
	void InitState();

protected:
	void AddState(EMoveState Type);
	void ApplyState(EMoveState Type);

protected:
	UFUNCTION()
		void OnCurrentStateChanged(EMoveState NewState);
	UFUNCTION()
		void OnLookingForPlayer();
	UFUNCTION()
		void OnWalk();
	UFUNCTION()
		void OnAttack();

private:
	TArray<FMoveState> States;
	FMoveState* CurrentState;
	bool bIsAttacking{ false };
};

inline void UMovementHandler::BeginPlay()
{
	Super::BeginPlay();
	InitState();
}

inline void UMovementHandler::InitState()
{
	AddState(EMovementState::EMS_LookingForTarget);
	CurrentState = &States.Top();
}

inline void UMovementHandler::AddState(EMovementState::Type)
{
	auto Enemy = dynamic_cast<AAIEnemy*>(GetOwner());
	if (Enemy)
	{
		States.AddUnique(FMoveState{ Type, dynamic_cast<AAIController*>(Char->GetController()) });
		States.Top().OnStateChanged.AddDynamic(this, &UMovementHandler::OnCurrentStateChanged)
	}
}

static FMoveState* GetState(TArray<FMoveState>& States, EMoveState Type)
{
	auto Result = States.FindByPredicate([=](FMoveState& State)
		{
			return (State.GetStateType() == Type)
		});
	return Result;
}

inline void UMovementHandler::ApplyState(EMovementState Type)
{
	CurrentState = GetState(States, Type);
	if (!CurrentState)
	{
		AddState(Type);
		CurrentState = &States.Top();
	}
	CurrentState->ApplyState();
}

inline void UMovementHandler::OnCurrentStateChanged(EMovementState NewState)
{
	if (NewState == EMovementState::EMS_LookingForTarget)
	{
		if (bIsAttacking)
			NewState = EMovementState::EMS_Attack;
		else
			NewState = EMovementState::EMS_Walk;
	}
	ApplyState(NewState);
}

inline void UMovementHandler::OnLookingForPlayer()
{
	if (CurrentState && CurrentState->GetStateType() != EMovementState::EMS_Attack)
	{
		ApplyState(EMovementState::EMS_LookingForTarget);
		bIsAttacking = false;
	}
}

inline void UMovementHandler::OnWalk()
{
	if (CurrentState && CurrentState->GetStateType() != EMovementState::EMS_Attack)
	{
		ApplyState(EMovementState::EMS_Walk);
		bIsAttacking = false;
	}
}

inline void UMovementHandler::OnAttack()
{
	if (CurrentState)
	{
		ApplyState(EMovementState::EMS_Attack);
		bIsAttacking = true;
	}
}