#pragma once

#include "Subject.generated.h"

class Subject
{
private:
	TAraay <Observer*> observers;
	
public:
	void Subscribe(Observer* observer);
	void Unsubscribe(Observer* observer);
	void Broadcast();
};

inline Subject::Subscribe(Observer* observer)
{
	observers.Emplace(observer);
}

inline Subject::Unsubscribe(Observer* observer)
{
	observers.RemoveSingle(observer);
}

inline Subject::Broadcast()
{
	for (auto* Observer : observers)
	{
		Observer.onLvlUp();
	}
}