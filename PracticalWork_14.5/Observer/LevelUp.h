#pragma once

#include "LevelUp.generated.h"

UCLASS()
class ULevelUp : public Observer
{
public:
	virtual void onLvlUp() override
	{
		// here we will perform all the necessary manipulations related to raising the level of the character:
		// increasing characteristics, unlocking skills, activating achievements, and so on
		// the same thing will happen in other classes subscribed to the event
	}
};
