#pragma once

#include "Observer.generated.h"

UCLASS()
class Observer
{
	virtual void onLvlUp() = 0;
};